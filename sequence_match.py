#!/usr/bin/env python
"""Split input fastq files by matching sequence ids against those in the specified fastq file (match_sequences.fastq).

IDs are matched ignoring read number to support multiple reads (e.g. from Illumina multiplexing).

Fastq files must have matching reads in the same order.

If only one file is specified, matches go to stdout, non-matches to stderr.
Otherwise, matches go to input_matched.fastq and input_unmatched.fastq.
"""
from Bio import SeqIO
from Bio.SeqIO.QualityIO import FastqGeneralIterator
from fastq_utils import fastq_utils
import optparse
import os
import sys



__version__ = "0.1"
__author__ = "Lance Parsons"
__author_email__ = "lparsons@princeton.edu"
__copyright__ = "Copyright 2011, Lance Parsons"
__license__ = "BSD 2-Clause License http://www.opensource.org/licenses/BSD-2-Clause"



def main (argv=None):
    if argv is None:
        argv = sys.argv
    
    # TODO Test BioPython Version (1.57+)
    
    usage = "Usage: %prog [options] match_sequences.fastq input_1.fastq [input_2.fastq] [input_3.fastq] ..."
    parser = optparse.OptionParser(usage=usage, version='%prog version ' + globals()['__version__'], description=globals()['__doc__'])
    parser.add_option ('-v', '--verbose', action='store_true', default=False, help='verbose output')
    parser.add_option ('--id_format', default=None, help='id format: illumina, stripone, or other')
    parser.add_option ('--index_file', default=None, help='Name of index file')
    parser.add_option ('--save_index', action='store_true', default=False, help='save index (usually for debug)')
    try:
        (options, args) = parser.parse_args(argv[1:])
        if len(args) < 2:
            parser.error('Must specify at least match_sequences fastq file and one input fastq file')
    except SystemExit: # Prevent exit when calling as function
        return 2
    

    reads_to_match_filename = args[0]
    if options.verbose:
        print "Match reads in file: %s" % reads_to_match_filename
    
    index_filename = "%s.idx" % reads_to_match_filename
    if options.index_file is not None:
        index_filename = options.index_file
    
    # Determine ID format
    if options.id_format not in (fastq_utils.ILLUMINA, fastq_utils.STRIPONE, fastq_utils.OTHER):
        if options.id_format is not None:
            sys.stderr.write("Unknown ID format specified: '%s'\nAttempting to determine automatically\n" % options.id_format)
        fh = open(reads_to_match_filename, 'rb')
        first_line = fh.readline().strip()
        id_format = fastq_utils.determine_id_format(first_line[1:])
        fh.close() 
    else:
        id_format = options.id_format
    
    # Prepare lists of files                
    input_files = args[1:]
    input_filehandles = {}
    fastq_generators = {}
    output_files = {}
    for i in range(len(input_files)):
        if options.verbose:
            print "Input File: %s" % input_files[i]
        (root, ext) = os.path.splitext(input_files[i])
        input_filehandles[i] = open(input_files[i], 'rb')
        fastq_generators[i] = FastqGeneralIterator(input_filehandles[i])
        if len(input_files) == 1:
            output_files[i] = {'matched': sys.stdout, 'unmatched': sys.stderr}
        else:
            output_files[i] = {'matched': open("%s_matched%s" % (root, ext), 'wb'), 'unmatched': open("%s_unmatched%s" % (root, ext), 'wb')}
    
    # Create index
    if options.verbose:
        print "Building index of reads in '%s'" % reads_to_match_filename
    if id_format == fastq_utils.ILLUMINA:
            index = SeqIO.index_db(index_filename, reads_to_match_filename, "fastq", key_function=fastq_utils.strip_read_from_id_illumina)
    elif id_format == fastq_utils.STRIPONE:
            index = SeqIO.index_db(index_filename, reads_to_match_filename, "fastq", key_function=fastq_utils.strip_read_from_id_stripone)
    else:
            index = SeqIO.index_db(index_filename, reads_to_match_filename, "fastq")
    
    # Loop through records
    if options.verbose:
        print "Matching reads in %s" % input_files[0]
    for (seq_id, seq, qual) in fastq_generators[0]:
        match_id = fastq_utils.strip_read_from_id(seq_id, id_format)
        match = False
        if match_id in index:
            match = True
            output_files[0]['matched'].write("@%s\n%s\n+\n%s\n" % (seq_id, seq, qual))
        else:
            output_files[0]['unmatched'].write("@%s\n%s\n+\n%s\n" % (seq_id, seq, qual))
            
        for i in range(1,len(input_filehandles)):
            (seq_id, seq, qual) = fastq_generators[i].next()
            this_match_id = fastq_utils.strip_read_from_id(seq_id, id_format)
            assert this_match_id == match_id
            if match:
                output_files[i]['matched'].write("@%s\n%s\n+\n%s\n" % (seq_id, seq, qual))
                pass
            else:
                output_files[i]['unmatched'].write("@%s\n%s\n+\n%s\n" % (seq_id, seq, qual))
    
    # Remove index file
    if not options.save_index:
        os.unlink(index_filename)
        
    return 0
    

if __name__ == '__main__':
    sys.exit(main())