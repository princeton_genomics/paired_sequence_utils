'''
Created on May 31, 2012

@author: lparsons
'''

from distutils.core import setup

#import sys
#try:
    # Load setuptools, to build a specific source package
#    from setuptools import setup
#except ImportError:
#    from distutils.core import setup


setup_args = dict(
    name="paired_sequence_utils",
    version="0.1",
    #packages=['paired_sequence_utils'],
    
    py_modules = ['paired_sequence_match', 'barcode_splitter'],
      
    scripts = ['barcode_splitter.py', 'paired_sequence_match.py'],

    data_files=[('', ['LICENSE.txt'])],
    
    # metadata for upload to PyPI
    author="Lance Parsons",
    author_email="lparsons@princeton.edu",
    description="Utilities to work with paired sequence files",
    license="BSD 2-clause",
    keywords="sequencing fastq paired barcode",
    url="https://bitbucket.org/lance_parsons/paired_sequence_utils",

    # could also include long_description, download_url, classifiers, etc.
)


#if 'setuptools' in sys.modules:
#    setup_args['install_requires']=[
#        'BioPython>=1.57',
#    ],
    #entry_points={
    #     'console_scripts': [
    #     'barcode_splitter = paired_sequence_utils.barcode_splitter:main',
    #      'paired_sequence_match = paired_sequence_utils.paired_sequence_match:main',
    #    ],
    #},
setup(**setup_args)