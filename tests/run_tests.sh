#!/bin/bash

# Run functional tests for paired_sequence_match.py
TEST_DATA=test_data

function check_test_results {
for OUTPUT in $OUTPUTS; do
	diff "${TEST_DATA}/${TEST}_${OUTPUT}.out" "${TEST_DATA}/${TEST}_${OUTPUT}.fastq"
	if [ $? -ne 0 ];then
		echo "${TEST} ${OUTPUT} FAILED!"
	else
		echo "${TEST} ${OUTPUT} PASSED"
	fi
done
}

# Test split file output
TEST=test_1
OUTPUTS="paired1 paired2 singles" 
rm -f "${TEST_DATA}\${TEST}*.out"
paired_sequence_match "${TEST_DATA}/${TEST}_read1.fastq" "${TEST_DATA}/${TEST}_read2.fastq" -p "${TEST_DATA}/${TEST}_paired1.out" -p "${TEST_DATA}/${TEST}_paired2.out" -s "${TEST_DATA}/${TEST}_singles.out"
if [ $? -ne 0 ];then
	echo "${TEST} ${OUTPUT} FAILED!"
else
	check_test_results
fi


# Test Interleaved paired output
TEST=test_2
OUTPUTS="interleaved singles" 
rm -f ${TEST_DATA}\${TEST}*.out
paired_sequence_match ${TEST_DATA}/${TEST}_read1.fastq ${TEST_DATA}/${TEST}_read2.fastq -p ${TEST_DATA}/${TEST}_interleaved.out -s ${TEST_DATA}/${TEST}_singles.out
if [ $? -ne 0 ];then
	echo "${TEST} ${OUTPUT} FAILED!"
else
	check_test_results
fi

# Test Illumina CASAVA 1.8+ style headers
TEST=test_3
OUTPUTS="interleaved singles" 
rm -f ${TEST_DATA}\${TEST}*.out
paired_sequence_match ${TEST_DATA}/${TEST}_read1.fastq ${TEST_DATA}/${TEST}_read2.fastq -p ${TEST_DATA}/${TEST}_interleaved.out -s ${TEST_DATA}/${TEST}_singles.out --id-split " "
if [ $? -ne 0 ];then
	echo "${TEST} ${OUTPUT} FAILED!"
else
	check_test_results
fi

