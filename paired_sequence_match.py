#!/usr/bin/env python
"""Takes two sequence files as input and matches up paired sequences, outputting them separately from orphan sequences.
Useful when paired reads are in two separate files and were filtered separately. By default, paired reads are output 
interleaved with another (read 1 and read 2 of a pair, then read 1 and read 2 of a second pair, etc.).  If the 
paired output file is specified twice, the first read is output to the in the first file, the second read of a pair is
output in the second file. 
"""

__version__ = "0.1"
__author__ = "Lance Parsons"
__author_email__ = "lparsons@princeton.edu"
__copyright__ = "Copyright 2011, Lance Parsons"
__license__ = "BSD 2-Clause License http://www.opensource.org/licenses/BSD-2-Clause"


import sys, os, optparse
import time
import fileinput
import gzip
from Bio.SeqIO.QualityIO import FastqGeneralIterator
from Bio import SeqIO

def main (argv=None):
    if argv is None:
        argv = sys.argv

    global options, args, interleaved, paired_output, single_output
    
    usage = "Usage: %prog [options] read1_sequence_file read2_sequence_file"
    parser = optparse.OptionParser(usage=usage, version='%prog version ' + globals()['__version__'], description=globals()['__doc__'])
    parser.add_option ('-p', '--paired-output-files', action='append', help='output file(s) for paired reads')
    parser.add_option ('-s', '--single-output-file', help='output file for single reads')
    parser.add_option ('-i', '--id-split', default="/", help='Charater to use to split id (default: "%default")')
    parser.add_option ('-m', '--index-in-memory', 
                       action='store_true',
                       default=False, 
                       help='Store index in memory instead on disk (default: "%default")')
    #parser.add_option ('-f', '--format', default="fastq", help='sequence file format [fastq or fasta] (default: %default)')
    parser.add_option ('-v', '--verbose', action='store_true', default=False, help='verbose output')
    parser.add_option ('-z', '--gzip', action='store_true', default=False, help='compress output with GZIP')
    (options, args) = parser.parse_args()
    if len(args) < 2:
        parser.error ('Please specify two sequence files')

    # Setup input file handles    
    f_file = fileinput.FileInput(args[0], openhook=fileinput.hook_compressed)
    r_file = fileinput.FileInput(args[1], openhook=fileinput.hook_compressed)
    f_fastq = FastqGeneralIterator(f_file)
    r_fastq = FastqGeneralIterator(r_file)
    matched_pair_count = 0
    single_count = 0

    # Setup output file handles
    interleaved = True
    if (options.paired_output_files):
        if options.verbose: print "Output pairs to: %s" % options.paired_output_files
        if len(options.paired_output_files) == 1: # Python 3: isinstance(arg, str)
            interleaved = True
            if (options.gzip):
                paired_output = gzip.open(options.paired_output_files[0], "wb")
            else:
                paired_output = open(options.paired_output_files[0], "wb")
        else:
            interleaved = False
            paired_output = [None, None]
            if (options.gzip):
                paired_output[0] = gzip.open(options.paired_output_files[0], "wb")
                paired_output[1] = gzip.open(options.paired_output_files[1], "wb")
            else:
                paired_output[0] = open(options.paired_output_files[0], "wb")
                paired_output[1] = open(options.paired_output_files[1], "wb")
    else:
        if (options.gzip):
            paired_output = gzip.GzipFile(fileobj=sys.stdout, mode="wb")
        else:
            paired_output = sys.stdout
    
    if (options.single_output_file):
        if (options.gzip):
            single_output = gzip.GzipFile(options.single_output_file, "wb")
        else:
            single_output = open(options.single_output_file, "w")
    else:
        if (options.gzip):
            single_output = gzip.GzipFile(fileobj=sys.stderr, mode="wb")
        else:
            single_output = sys.stderr
    
    # Create reverse read index
    r_file_index = "%s.idx" % args[1]
    if options.index_in_memory: r_file_index = ':memory:'
    if options.verbose:
        start = time.time()
        print "Building index of reads in '%s'" % args[1]
    try:
        r_index = SeqIO.index_db(r_file_index, args[1], "fastq", key_function=split_id)
    except:
        sys.stderr.write("Problem during index creation, aborting\n")
        if os.path.exists(r_file_index): os.unlink(r_file_index)
        raise
    if options.verbose: print "Index built in %s mins" % ((time.time() - start)
                                                          / 60.0)
    
    for (f_id, f_seq, f_q) in f_fastq:
        match_id = split_id(f_id)
        if match_id in r_index:
            r_read = r_index.get_raw(match_id)
            print_match("@%s\n%s\n+\n%s\n" % (f_id, f_seq, f_q), r_read)
            matched_pair_count += 1
        else:
            single_output.write("@%s\n%s\n+\n%s\n" % (f_id, f_seq, f_q))
            single_count += 1
    
    # Create forward read index
    f_file_index = "%s.idx" % args[0]
    if options.index_in_memory: f_file_index = ':memory:'
    if options.verbose:
        start = time.time()
        print "Building index of reads in '%s'" % args[0]
    try:
        f_index = SeqIO.index_db(f_file_index, args[0], "fastq",
                                 key_function=split_id)
    except:
        sys.stderr.write("Problem during index creation, aborting\n")
        if os.path.exists(r_file_index): os.unlink(r_file_index)
        raise
    if options.verbose: print "Index built in %s mins" % ((time.time() - start)
                                                          / 60.0)
    
    for (r_id, r_seq, r_q) in r_fastq:
        match_id = split_id(r_id)
        if match_id not in f_index:
            single_output.write("@%s\n%s\n+\n%s\n" % (r_id, r_seq, r_q))
            single_count += 1

    if options.verbose: 
        print "Total Reads:\t%s\nMatched Pairs:\t%s\nSingle Reads:\t%s\n" % (matched_pair_count + single_count, matched_pair_count, single_count)
    
    if os.path.exists(r_file_index): os.unlink(r_file_index)
    if os.path.exists(f_file_index): os.unlink(f_file_index)

def split_id(seq_id):
    return seq_id.split(options.id_split)[0]

def print_match(f_read, r_read):
    global paired_output, interleaved
    if (interleaved):
        paired_output.write("%s%s" \
                            % (f_read, r_read))
    else:
        paired_output[0].write("%s" % f_read)
        paired_output[1].write("%s" % r_read)

if __name__ == '__main__':
    sys.exit(main())

'''
    try:
        start_time = time.time()
        usage = "Usage: %prog [options] read1_sequence_file read2_sequence_file"
        parser = optparse.OptionParser(usage=usage, version='%prog version ' + globals()['__version__'], description=globals()['__doc__'])
        parser.add_option ('-p', '--paired-output-files', action='append', help='output file(s) for paired reads')
        parser.add_option ('-s', '--single-output-file', help='output file for single reads')
        parser.add_option ('-i', '--id-split', default="/", help='Charater to use to split id (default: "%default")')
        parser.add_option ('-m', '--index-in-memory', 
                           action='store_true',
                           default=False, 
                           help='Store index in memory instead on disk (default: "%default")')
        #parser.add_option ('-f', '--format', default="fastq", help='sequence file format [fastq or fasta] (default: %default)')
        parser.add_option ('-v', '--verbose', action='store_true', default=False, help='verbose output')
        parser.add_option ('-z', '--gzip', action='store_true', default=False, help='compress output with GZIP')
        (options, args) = parser.parse_args()
        if len(args) < 2:
            parser.error ('Please specify two sequence files')
        if options.verbose: print time.asctime()
        main()
        if options.verbose: print time.asctime()
        if options.verbose: print 'TOTAL TIME IN MINUTES:',
        if options.verbose: print (time.time() - start_time) / 60.0
        sys.exit(0)
    except KeyboardInterrupt, e: # Ctrl-C
        raise e
    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
'''

